# README #
### What is this repository for? ###

* A remote surveillance system built using Tornado
* Implementation has been changed from Django to Tornado due to needs for periodiccalls.

### How do I get set up? ###

* Software:
- Tornado 4.0.1
- Torndb
- Python 2.8
- Picamera, PIL

* Hardware
- Raspberry Pi (as server)
- Picamera module
- Stepmotor
- Led lights