import picamera
import time
import io

class Camera:
	"""
	This class is a wrapper for PiCamera API and Stepmotor. If offers
	functions like capturing image with resolution and flash light, or 
	rotate the camera angle.
	"""
	#This is the angle of the camera
	__angle = 0
	#Resolution of camera pictures
	__resolution = (0,0)

	#stream variable for captured image
	#__cam_stream = io.BytesIO()

	#camera handler
	__camera = picamera.PiCamera()

	#location of used GPIO pins
	__led_pin = 23
	__motor_pins = [22,27,17,4]

	#Stepmotor variables
	waitDuration = 0.001
	step = 8

	Seq = range(0, step)
	Seq[0] = [1,0,0,0]
	Seq[1] = [1,1,0,0]
	Seq[2] = [0,1,0,0]
	Seq[3] = [0,1,1,0]
	Seq[4] = [0,0,1,0]
	Seq[5] = [0,0,1,1]
	Seq[6] = [0,0,0,1]
	Seq[7] = [1,0,0,1]

	#Settings
	#90 degrees is 1024 steps
	#45 degrees is 512 steps
	#5.625 degrees / 64 steps
	#1 degee is 11.4 steps
	oneDegree = 11
	
	def __init__(self, GPIO, angle, resolution):
		self.__angle = angle
		self.__resolution = resolution
		self.__GPIO = GPIO
		#Set pins controlling stepper motor as output
		for pin in self.__motor_pins:
			self.__GPIO.setup(pin,GPIO.OUT)
			self.__GPIO.output(pin, False)

		#Set led light pin as output		
		self.__GPIO.setup(self.__led_pin,GPIO.OUT)

	def __del__(self):
		self.__camera.close()

	def setResolution(self, resolution):
		self.__resolution = resolution

	def setLight(self, state):
		"""
		Set light using: on/off/toggle
		check light status using __cam_flash
		"""
		print("Camera: setlight")
		if state == "On" and not self.__GPIO.input(self.__led_pin):
			self.__GPIO.output(self.__led_pin, True)
		elif state == "Off" and self.__GPIO.input(self.__led_pin):
			self.__GPIO.output(self.__led_pin, False)
		elif state == "Toggle":
			self.__GPIO.output(self.__led_pin, not self.__GPIO.input(self.__led_pin))


	def captureImage(self, stream, flashOn=False, resolution=(640,480)):
		"""
		Capture an image and returns io.BytesIO-stream of it.
		stream: state the location where captured image shall be saved
		flashon: default False, set Led-light on or off before capture
		resolution: default empty tuple, bypass default resolution ie. (1280,720)
		"""
		t_res = self.__resolution
		if resolution:
			t_res = resolution

		
		#set light on if required
		t_init_lstate = self.__GPIO.input(self.__led_pin)
		if flashOn:
			print("Camera-capture: Light on")
			self.setLight("On")
			
		#capture image
		self.__camera.capture(stream, format='jpeg',resize=t_res)		
			
		#reset flash to previous state
		if flashOn and not t_init_lstate:
			print("Camera-capture: Light off")
			self.setLight("Off")
		
	def getAngle(self):
		return self.__angle;

	#This function sets the current camera angle as zero
	def resetAngle(self):
		self.__angle = 0

	def rotateLeft(self, angle):
		rotateAngle = angle * self.oneDegree
		self.__angle += angle
		print "Turning Right ", angle, " degrees"
		StepCounter = 0
		MoveCounter = 0
		# Start clockwise loop
		while MoveCounter <= rotateAngle:
			for pin in range(0, 4):
				xpin = self.__motor_pins[pin]
				if self.Seq[StepCounter][pin]!=0:
					self.__GPIO.output(xpin, True)
				else:
					self.__GPIO.output(xpin, False)
			StepCounter += 1
			MoveCounter += 1

  			# If we reach the end of the sequence
  			# start again
  			if (StepCounter==self.step):
    				StepCounter = 0
			if (StepCounter<0):
    				StepCounter = self.step

  			# Wait before moving on
  			time.sleep(self.waitDuration)



	def rotateRight(self, angle):
		rotateAngle = angle * self.oneDegree
		self.__angle -= angle
		print "Turning left ", angle, " degrees"
		StepCounter = 7
		MoveCounter = 0
		# Start counter-clockwise loop
		while MoveCounter <= rotateAngle:
			for pin in range(0, 4):
    				xpin = self.__motor_pins[pin]
    				if self.Seq[StepCounter][pin]!=0:
      					self.__GPIO.output(xpin, True)
    				else:
      					self.__GPIO.output(xpin, False)
  			StepCounter -= 1
  			MoveCounter += 1

  			# If we reach the end of the sequence
  			# start again
  			if (StepCounter < 0):
    				StepCounter = 7

  			# Wait before moving on
  			time.sleep(self.waitDuration)

if __name__ == "__main__":
	print("Nothing to see")
