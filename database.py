import time
import os
import hashlib
import uuid
import torndb
import json

class Database:
	
	def __init__(self):
		self._db = torndb.Connection("localhost", "db_tornado", "pi","kikkeliluola")
	def __del__(self):
		self._db.close()

	def get_last_10_images(self):
		"""
		Returns:
			Json of last 10 images or less if there are less images in the system. 
			Json has two parameters for each image: path and time
				path is of type "images/year/month/day/hh_mm_ss.jpg"
				time is of type "YYYY-MM-DD HH:MM:SS"
		"""
		q_string = "SELECT path, time FROM (SELECT * FROM image ORDER BY image_id DESC LIMIT 10) SUB ORDER BY image_id ASC;"
		dict = self._db.query(q_string)
		#response = []
		#for image in dict:
		#	path = image.path
		#	time = image.time.strftime("%Y-%m-%d %H:%M:%S")
		#	response.append({'path': path, 'time': time})
		#return response 		
		return dict
	
	def user_login(self, username, password):
		"""
		Parameters:
			username is string that contains username
			password is string that contains password in plain text format
		Returns:
			True if username and password were correct
			False if both or any of them are incorrect
		Operation:
			Updates database user that is login in last_login timestamp to now
		"""
		#This string is used to build mysql query string
		q_string = "SELECT username, password, salt FROM user;"
		#Here we have dictionary of passwords and usernames for all the users
		for user in self._db.query(q_string):
			#If username found from database
			if user.username == username:
				#Check if password is correct
				hash_pass = hashlib.sha512(password + user.salt).hexdigest()
				if hash_pass == user.password:
					#Username and password were correct
					return True
				else:
					return False
		#Username and/or password was incorrect
		return False
		
	def get_images(self, from_date, to_date, user):
		"""
		Parameters:
			from_date is DateTime that is in format "YYY-MM-DD"
			to_date is DateTime that is in format "YYYY-MM-DD"
			user is string that is either "all", "auto" or "username" where username 
				is the actual username of the user whose pictures are wanted
		Returns:
			Json that has two parameters for every image: path and time
				path is of type "images/year/month/day/hh_mm_ss.jpg"
				time is of type "YYYY-MM-DD HH:MM:SS"
		"""
		dict = {}
		#If toDate comes after fromDate then someone has made an impossible query
		if to_date < from_date :
			return dict
		#If all pictures are to be returned
		if user == "all":
			q_string = "SELECT path, time FROM image WHERE image.time BETWEEN \'"
			q_string += from_date + "\' AND \'" + to_date + "\';"
			dict = self._db.query(q_string)

		#If either automatically or user generated pictures are to be returned
		else:
			q_string = "SELECT path, time FROM image WHERE image.username=\'" + user
			q_string += "\' AND image.time BETWEEN \'" + from_date + "\' AND \'"
			q_string += to_date + "\';"
			dict = self._db.query(q_string)
		#response = []
		#for image in dict:
		#	path = image.path
		#	time = image.time.strftime("%Y-%m-%d %H:%M:%S")
		#	response.append({'path': path, 'time': time}) 		
		#return response
		#print(dict)
		return dict

	def save_image(stream, user):
		"""
		Paramers:
			stream is of type io.BytesIO and it contains one picture
			user is of type string and it contains the name of the user or in case
			it was automatically taken auto
		Operation:
			This function saves the given stream as .jpg file. The path that is used
			to save this file is static/Images/YYYY/MM/DD/ and the filename is named
			as HH_MM_SS.jpg .
		"""
		#Here we parse images full path
		dir_name = "static/Images/"
		file_name = date.hour + "_" + date.minute + "_" + date.second + ".jpg"
		#This is used to parse current date to the correct format(YYY/MM/DD)
		date = datetime.datetime.now().date()
		dir_name += date.year + "/" + date.month + "/" + date.day + "/"
		#This is the path that is saved to images path column so it can be used
		#in html files with the {{static_url(path)}} -macro
		sql_path = dir_name + file_name
		file_full_path = os.path.abspath(os.path.join(dir_name, file_name))
		#Check if directory exist and create it if neccesary
		dir_name = os.path.dirname(file_full_path)
		#This is the path that is saved to images path column so it can be used
		#in html files with the {{static_url(path)}} -macro
		sql_path = dir_name + file_name
		file_full_path = os.path.abspath(os.path.join(dir_name, file_name))
		#Check if directory exist and create it if neccesary
		dir_name = os.path.dirname(file_full_path)
		if not os.path.exists(dir_name):
			os.makedirs(dir_name)
		#Open file and write picture from stream to it
		open(file_full_path, 'wb').write(stream.getvalue())		
		#Next we make new row to database image table
		q_string = "INSERT INTO image VALUES (\"" + sql_path + "\", now(),\""
		#The last parameter in sql query, 0 is used just to make sure there are correct
		#amount of parameters. It is immediatly replaced by sql auto_increment with the
		#correct value.
		q_string += user + "\", 0);"
		self._db.query(q_string)
