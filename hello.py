import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import os
import io
import time
import tornado.gen
import camera
import RPi.GPIO as GPIO
import database
import json


from tornado.options import define, options
define("port", default=8181, help="run on the given port", type=int)

GPIO.setmode(GPIO.BCM)
stream = io.BytesIO()
cam = None

db = None

#Livefeed-watchdog variables
lref_interval = 1000 #in ms
lref_stayalive = 10 #in s
lref_stayalive_count = lref_stayalive
lref_running = False
lref_call = None # initialize in main

#log-page variables
log_max_lines = 20

class BaseHandler(tornado.web.RequestHandler):
	#TODO: Here put database check for user name and password
	def check_permissions(self, username, password):
		return db.user_login(username, password)
	#end TODO
	
	def set_current_user(self, user):
		if user:
			self.set_secure_cookie('user', tornado.escape.json_encode(user))
		else:
			self.clear_cookie('user')

	def get_current_user(self):
		return self.get_secure_cookie('user')
	

class LoginHandler(BaseHandler):
	#This is called when loading first page
	def get(self):
		if not self.current_user:
			self.render('jarval/login.html', wrongPass='False', firstTime='True')
			return
		self.render('jarval/index.html', userName=self.current_user)
					
	#This is called when in login page submit button is pressed
	def post(self):
		user = self.get_argument('username')
		passw = self.get_argument('password')
		auth = self.check_permissions(user, passw)		
		#TODO: GEt users First name from database and authenticate him with that
		if auth:
			self.set_current_user(user)
			self.render('jarval/index.html', userName=user)
		else:
			self.render('jarval/login.html', wrongPass='True', firstTime='False')
	
class LogoutHandler(BaseHandler):
	def get(self):
		self.clear_cookie('user')
		self.redirect('/')

class HomeHandler(BaseHandler):
	def get(self):
		if not self.current_user:
			self.redirect('/')
			return
		self.render('jarval/index.html', userName=self.current_user)

class LogHandler(BaseHandler):
	def get(self):
		if not self.current_user:
			self.redirect("/")
			return
		print(self.current_user+" ("+self.request.remote_ip+"): log access")
		today = time.strftime("%Y-%m-%d")
		self.render('jarval/log.html', userName=self.current_user,
					system_date_today=today, max_lines=log_max_lines )

class SettingsHandler(BaseHandler):
	def get(self):
		if not self.current_user:
			self.redirect("/")
			return
		print(self.current_user+" ("+self.request.remote_ip+"): settings access")
		self.render('jarval/settings.html', userName=self.current_user)

class GalleryHandler(BaseHandler):
	def get(self):
		if not self.current_user:
			self.redirect("/")
			return
		print(self.current_user+" ("+self.request.remote_ip+"): gallery access")
		today = time.strftime("%Y-%m-%d")
		dict = db.get_last_10_images()
		self.render('jarval/gallery.html', 
					userName=self.current_user, 
					system_date_today=today,
					dict = dict)

	def post(self):
		if not self.current_user:
			self.redirect("/")
			return
		data = json.loads(self.request.body.decode("utf-8"))
		print(data)
		filter_from = data["filter_from"]
		filter_to = data['filter_to']
		cap_mo = data['capture_mode']
		user = ""
		if cap_mo == "automatic":
			user = "auto"
		elif cap_mo == "both":
			user = "all"
		elif cap_mo == "manual":
			user = self.current_user[1:-1]
			print("User in GalleryHandler post request: " + user )
		else:
			return
		print(self.current_user+" ("+self.request.remote_ip+"): gallery filter with ( from: "+filter_from+" to "+filter_to+" and mode: "+cap_mo)
		dict = db.get_images(filter_from, filter_to, user)
		#today = time.strftime("%Y-%m-%d")
		response = []
		for image in dict:
			path = image.path
			time = image.time.strftime("%Y-%m-%d %H:%M:%S")
			response.append({'path': path, 'time': time})
		self.write(json.dumps(response))
		
		"""self.render('jarval/gallery.html', 
					userName=self.current_user, 
					system_date_today=today,
					dict = dict)"""		

class RotateHandler(BaseHandler):
	def get(self, input):
		if not self.current_user:
			self.redirect("/")
			return
		t_input = ""
		if input == "left":
			t_input = "rotate "+input
			cam.rotateLeft(10)
		elif input == "right":
			t_input = "rotate "+input
			cam.rotateRight(10)
		elif input == "light":
			t_input = "set "+input
			cam.setLight("Toggle") #3 states to call: On, Off or Toggle
		print(self.current_user+" ("+self.request.remote_ip+"): "+t_input)
		

#This fuction is called by main function every x seconds
def refreshImageStream():
	global lref_running
	global lref_stayalive
	global lref_stayalive_count
	if lref_stayalive_count >= 0:
		stream.seek(0)
		stream.truncate()
		cam.captureImage(stream,False,(640,480))
		lref_stayalive_count -= 1
		#print time.strftime("%H:%M:%S")
	else:
		lref_call.stop()
		lref_stayalive_count = lref_stayalive
		lref_running = False

#LiveFeed Refresh callback
lref_call = tornado.ioloop.PeriodicCallback(refreshImageStream, lref_interval)


class StreamHandler(BaseHandler):
	def get(self):
		if not self.current_user:
			self.redirect("/")
			return
		global lref_running
		global lref_stayalive_count
		if lref_running == False:
			lref_call.start()
			lref_running = True
		else:
			lref_stayalive_count = lref_stayalive

		self.set_header('Content-type', 'image/jpg')
		s = stream.getvalue()
		self.set_header('Content-length', len(s))
		self.write(s)
		self.flush()


class FilterHandler(tornado.web.RequestHandler):
	"""def get(self, input):
		if input[0:3] == "log":
			print("log kutsutttu")
		elif input[0:7] == "gallery":
			print("gallery kutsuttu")
			from_date = input[7:]
			to_date = from_date[8:]
			from_date = from_date[:8]
			print(from_date)
			print(to_date)
			to_date = to_date[:4] + "-" + to_date[4:2] + "-" + to_date[6:]
			print(to_date)
	"""
	def post(self):
		print("Got Json data: ", data)
		print(data["filter_from"])
		print(data["capture_mode"])
		dict = db.get_images
		self.write({"jee": "Hejsan"})


if __name__ == "__main__":
	#logger.setLevel(logging.DEBUG)
	app = tornado.web.Application(handlers=[(r"/", LoginHandler), 
											(r"/home", HomeHandler),
											(r"/gallery", GalleryHandler),
											(r"/logout", LogoutHandler),
											(r"/log", LogHandler),
											(r"/settings", SettingsHandler),
											(r"/stream", StreamHandler),
											(r"/rotate/(\w+)", RotateHandler),
											(r"/css/(.*)", tornado.web.StaticFileHandler, {'path': './jarval/css'}),
											(r"/js/(.*)", tornado.web.StaticFileHandler, {'path': './jarval/js'}),
											(r"/filter", FilterHandler),
								
											], static_path=os.path.join(os.path.dirname(__file__),"static"),
											cookie_secret="p82348023urjwn2elhtui23h5it3m2fipj23pt", debug=True)
	http_server = tornado.httpserver.HTTPServer(app)
	http_server.listen(options.port)

	try:
		#Creating database object
		global db
		db = database.Database()

		print("Setup camera")
		cam = camera.Camera(GPIO,0,(640,480))
		
		#force camera light off
		cam.setLight("Off")
		print("Server started")
		tornado.ioloop.IOLoop.instance().start()
	except:
		GPIO.cleanup()

