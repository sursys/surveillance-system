import pygame.camera
import pygame.image
import time

pygame.camera.init()
cam = pygame.camera.Camera(pygame.camera.list_cameras()[0])
cam.start()
stoptime = 10
while stoptime > 0 :
	img = cam.get_image()
	pygame.image.save(img, "../images/image.jpg")	
	time.sleep(5)
	stoptime -= 1
pygame.camera.quit()
